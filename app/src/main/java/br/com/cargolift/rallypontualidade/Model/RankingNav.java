package br.com.cargolift.rallypontualidade.Model;

/**
 * Created by paulogabriel on 22/11/2017.
 */

public class RankingNav{

    private String Eficiencia;

    private String Sem_Inconsistencia;

    private String Colocacao;

    private String CPF;

    private String Com_Inconsistencia;

    private String Nome;

    private int Total;

    public String getEficiencia ()
    {
        return Eficiencia;
    }

    public void setEficiencia (String Eficiencia)
    {
        this.Eficiencia = Eficiencia;
    }

    public String getSem_Inconsistencia ()
    {
        return Sem_Inconsistencia;
    }

    public void setSem_Inconsistencia (String Sem_Inconsistencia)
    {
        this.Sem_Inconsistencia = Sem_Inconsistencia;
    }

    public String getColocacao ()
    {
        return Colocacao;
    }

    public void setColocacao (String Colocacao)
    {
        this.Colocacao = Colocacao;
    }

    public String getCPF ()
    {
        return CPF;
    }

    public void setCPF (String CPF)
    {
        this.CPF = CPF;
    }

    public String getCom_Inconsistencia ()
    {
        return Com_Inconsistencia;
    }

    public void setCom_Inconsistencia (String Com_Inconsistencia)
    {
        this.Com_Inconsistencia = Com_Inconsistencia;
    }

    public String getNome ()
    {
        return Nome;
    }

    public void setNome (String Nome)
    {
        this.Nome = Nome;
    }

    public int getTotal ()
    {
        return Total;
    }

    public void setTotal (int Total)
    {
        this.Total = Total;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Eficiencia = "+Eficiencia+", Sem_Inconsistencia = "+Sem_Inconsistencia+", Colocacao = "+Colocacao+", CPF = "+CPF+", Com_Inconsistencia = "+Com_Inconsistencia+", Nome = "+Nome+", Total = "+Total+"]";
    }
}