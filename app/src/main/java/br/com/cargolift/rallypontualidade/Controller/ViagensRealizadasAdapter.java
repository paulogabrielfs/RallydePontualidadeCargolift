package br.com.cargolift.rallypontualidade.Controller;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import br.com.cargolift.rallypontualidade.Model.Auditoria;
import br.com.cargolift.rallypontualidade.Model.Ranking;
import br.com.cargolift.rallypontualidade.R;
import br.com.cargolift.rallypontualidade.util.FormatarData;


/**
 * Created by paulogabriel on 06/11/2017.
 */

public class ViagensRealizadasAdapter extends BaseAdapter {

    private final List<Auditoria> audit;
    private final Activity act;

    public ViagensRealizadasAdapter(List<Auditoria> audit, Activity act) {
        this.audit = audit;
        this.act = act;
    }
    @Override
    public int getCount() {
        return audit.size();
    }

    @Override
    public Object getItem(int position) {
        return audit.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.list_auditoria_viagens_realizadas, parent, false);

        Auditoria auditi = audit.get(position);

        TextView placa_frota = (TextView)
                view.findViewById(R.id.placa_frota);
        TextView origem = (TextView)
                view.findViewById(R.id.origem);
        TextView pontuacao1 = (TextView)
                view.findViewById(R.id.pontuacao_1);
        TextView data_inicio = (TextView)
                view.findViewById(R.id.data_inicio);
        TextView data_fim = (TextView)
                view.findViewById(R.id.data_fim);

        double pontuacao = (auditi.getPicos_Valor() + auditi.getTrabalhados_Valor() + auditi.getVelocidade_Valor() + auditi.getSaida_Origem() + auditi.getChegada_Destino() + auditi.getSaida_Destino());
        String pontuacaos = String.valueOf(pontuacao);
        placa_frota.setText(String.valueOf(auditi.getPlaca()));
        origem.setText(auditi.getLocal());
        String nota;
        try{
            nota = String.valueOf(pontuacaos).substring(0, 4);
        }
        catch (Exception e){
            nota = String.valueOf(pontuacaos).substring(0, 2);
        }
        pontuacao1.setText(nota);
        try {
            data_inicio.setText(String.valueOf(FormatarData.formatardata(auditi.getData_Prevista())));
            data_fim.setText(String.valueOf(FormatarData.formatardata(auditi.getData_Realizada())));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return view;
    }
}
