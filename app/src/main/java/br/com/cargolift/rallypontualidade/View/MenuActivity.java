package br.com.cargolift.rallypontualidade.View;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import br.com.cargolift.rallypontualidade.Controller.RankNavClassAdapter;
import br.com.cargolift.rallypontualidade.Model.RankingNav;
import br.com.cargolift.rallypontualidade.util.NetworkState;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import br.com.cargolift.rallypontualidade.Controller.MainClassAdapter;
import br.com.cargolift.rallypontualidade.Data.ContratoDB;
import br.com.cargolift.rallypontualidade.Model.Periodo;
import br.com.cargolift.rallypontualidade.Model.Ranking;
import br.com.cargolift.rallypontualidade.R;
import br.com.cargolift.rallypontualidade.Sync.SyncAdapter;
import br.com.cargolift.rallypontualidade.util.FormatarData;
import br.com.cargolift.rallypontualidade.util.NetworkState;

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private TextView finalResult;
    private Button button;
    private ListView lista;
    private List<Ranking> rank;
    private List<RankingNav> rankNav;
    private TextView Colocacao;
    private TextView DiasTrabalhados;
    private TextView Nota;
    private TextView Nota2;
    private Uri BuscarUltimoPeriodo = ContratoDB.PeriodoEntry.buildPeriodoUri();
    private Cursor cursorPeriodo;
    private Cursor cursorPeriodo2;
    private Context test;
    private String Periodo;
    private String CPFMOT;
    private String NomeMot;
    private ProgressBar progresso;
    private LinearLayout LLperiodo;
    private TextView TV_PONTUALIDADE;
    private TextView MinhaNota;
    TextView name;

    private static final String[] Periodo_COLUMNS = {
            "RPe_Codigo"
    };

    private static final String[] Periodo_COLUMNS2 = {
            ContratoDB.PeriodoEntry.TABLE_NAME +"."+
                    ContratoDB.PeriodoEntry._ID,
            ContratoDB.PeriodoEntry.RPe_Codigo,
            ContratoDB.PeriodoEntry.RPe_Descricao,
            ContratoDB.PeriodoEntry.RPe_Dat_Inicio,
            ContratoDB.PeriodoEntry.RPe_Dat_Fim,
            ContratoDB.PeriodoEntry.RPe_Data
    };

    static final int COL_PERIODO_ID = 0;
    static final int COL_PERIODO_CODIGO = 1;
    static final int COL_PERIODO_DESCRICAO = 2;
    static final int COL_PERIODO_DAT_INI = 3;
    static final int COL_PERIODO_DAT_FIM = 4;
    static final int COL_PERIODO_DAT = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            progresso = (ProgressBar) findViewById(R.id.PBcarregando);
            Colocacao = (TextView) findViewById(R.id.TV_colocacao);
            DiasTrabalhados = (TextView) findViewById(R.id.TV_trabalhados);
            Nota = (TextView) findViewById(R.id.TV_notaum);
            Nota2 = (TextView) findViewById(R.id.TV_notadois);
            MinhaNota = (TextView) findViewById(R.id.TextNota);

            TV_PONTUALIDADE = (TextView) findViewById(R.id.TV_PONTUALIDADE);
            Nota2.setVisibility(View.GONE);
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.requestLayout();
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_menu);
            navigationView.bringToFront();
            navigationView.setNavigationItemSelectedListener(this);

            View header=navigationView.getHeaderView(0);
            //View view = navigationView.inflateHeaderView(R.layout.nav_header_menu);
            name = header.findViewById(R.id.Nome);

            Menu menu = navigationView.getMenu();
            MenuItem tools1= menu.findItem(R.id.nav_ranking);
            SpannableString s1 = new SpannableString(tools1.getTitle());
            s1.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s1.length(), 0);
            tools1.setTitle(s1);
            MenuItem tools2= menu.findItem(R.id.nav_auditoria);
            SpannableString s2 = new SpannableString(tools2.getTitle());
            s2.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s2.length(), 0);
            tools2.setTitle(s2);
            MenuItem tools3= menu.findItem(R.id.nav_regulamento);
            SpannableString s3 = new SpannableString(tools3.getTitle());
            s3.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s3.length(), 0);
            tools3.setTitle(s3);
            MenuItem tools4= menu.findItem(R.id.nav_sair);
            SpannableString s4 = new SpannableString(tools4.getTitle());
            s4.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s4.length(), 0);
            tools4.setTitle(s4);
            navigationView.setNavigationItemSelectedListener(this);



            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.abs_layout);

            lista = (ListView) findViewById(R.id.RankingList);
            SyncAdapter.initializeSyncAdapter(this);

            LLperiodo = (LinearLayout) findViewById(R.id.LLperiodo);
            LLperiodo.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(MenuActivity.this, PeriodoActivity.class);
                    startActivityForResult(i, 1);
                }}
            );
            SharedPreferences sp = this.getSharedPreferences("periodo", MODE_PRIVATE);

            Periodo = sp.getString("cdperiodo", "");

            if (Objects.equals(Periodo, "")){

                cursorPeriodo = getApplicationContext().getContentResolver().query(
                        BuscarUltimoPeriodo, Periodo_COLUMNS, null, null, "_ID ASC");

                int contador = cursorPeriodo.getCount();

                cursorPeriodo.moveToLast();

                Periodo = cursorPeriodo.getString(0);

            }

            String SelectItemWithCodPeriodo = ContratoDB.PeriodoEntry.RPe_Codigo + " = ?";

            String[] mSELECT_TTF_ARGUMENTS = {""};

            mSELECT_TTF_ARGUMENTS[0] = Periodo;

            cursorPeriodo2 = getApplicationContext().getContentResolver().query(
                    BuscarUltimoPeriodo, Periodo_COLUMNS2, SelectItemWithCodPeriodo, mSELECT_TTF_ARGUMENTS, "_ID ASC");

            if (cursorPeriodo2 != null) {
                cursorPeriodo2.moveToFirst();
            }


            try {

                String data = FormatarData.formatardata(cursorPeriodo2.getString(COL_PERIODO_DAT_INI)) + " a " + FormatarData.formatardata(cursorPeriodo2.getString(COL_PERIODO_DAT_FIM));
                TextView TV_DATA = (TextView) findViewById(R.id.TV_DATA);

                TV_DATA.setText(data);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        SharedPreferences pref = getApplicationContext().getSharedPreferences("Motorista", 0);
        String NomeMot2 = pref.getString("CPF", null);
        if (!Objects.equals(NomeMot2, "") && NomeMot2 != null){
            NomeMot = pref.getString("Nome", null);
            CPFMOT = pref.getString("CPF",null);
            name.setText(NomeMot);
            AsyncTaskRunner runner = new AsyncTaskRunner();
            String sleepTime = "30";
            runner.execute(sleepTime);
        }
        SharedPreferences pref2 = getApplicationContext().getSharedPreferences("Navegador", 0);
        String cpf = pref2.getString("CPF", null);
        if (!Objects.equals(cpf, "") && cpf != null) {
            CPFMOT = pref2.getString("CPF", null);
            MinhaNota.setText("EFICIÊNCIA:");
            AsyncTaskRunnerNav runner = new AsyncTaskRunnerNav();
            String sleepTime = "30";
            runner.execute(sleepTime);
        }




    }
    public class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;
        AlertDialog.Builder alertDialog;

        @Override
        protected String doInBackground(String... params) {

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            if (NetworkState.isInternetAvailable_Rally()){


            try {

                URL url = new URL("http://wsrally.cargolift.com.br:50740/api/ClassificacaoMot?rpe_codigo="+ Periodo + "&cpf="+ CPFMOT);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String linha;
                StringBuffer buffer = new StringBuffer();
                while((linha = reader.readLine()) != null) {
                    buffer.append(linha);
                    buffer.append("\n");
                }

                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }

                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                int time = Integer.parseInt(params[0]) * 1000;

                Thread.sleep(time);
                resp = "Slept for " + params[0] + " seconds";
            } catch (InterruptedException e) {
                e.printStackTrace();
                resp = e.getMessage();
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            }else {
                resp = "noconn";
            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            if (Objects.equals(result, "noconn")){
                new AlertDialog.Builder(MenuActivity.this)
                        .setTitle("Erro de conexão")
                        .setMessage("Houve um problema ao tentar se conectar aos nossos servidores")
                        .setPositiveButton("Tentar Novamente", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(MenuActivity.this, MenuActivity.class);
                                startActivity(intent);
                            }
                        }).setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        System.exit(0);
                    }
                }).show();
            }else {
                Gson gson = new Gson();
                Type type = new TypeToken<List<Ranking>>() {
                }.getType();
                List<Ranking> fromJson = gson.fromJson(result, type);

                for (Ranking task : fromJson) {
                    if (Objects.equals(task.getNome(), NomeMot)) {
                        Colocacao.setText(String.valueOf(task.getColocacao()));
                        String notapontualidade = String.valueOf(task.getSaida_Destino() + task.getChegada_Destino() + task.getSaida_Origem()).substring(0, 2).replaceAll("\\.", "") + "/35";
                        DiasTrabalhados.setText(notapontualidade);
                        Nota.setText(String.valueOf(task.getNota()).substring(0, 2));
                        String nota2;
                        try {
                            nota2 = "," + String.valueOf(task.getNota()).substring(3, 5);
                        } catch (Exception e) {
                            try {
                                nota2 = "," + String.valueOf(task.getNota()).substring(3, 4) + "0";
                            } catch (Exception f) {
                                nota2 = ",00";
                            }
                        }
                        Nota2.setText(nota2);
                        Nota2.setVisibility(View.VISIBLE);
                    }
                }
                // execution of result of Long time consuming operation
                progresso.setVisibility(View.GONE);
                LLperiodo.setVisibility(View.VISIBLE);
                lista.setVisibility(View.VISIBLE);
                rank = fromJson;
                MainClassAdapter adapter = new MainClassAdapter(rank, MenuActivity.this);
                lista.setAdapter(adapter);
            }
        }


        @Override
        protected void onPreExecute() {
            progresso.setVisibility(View.VISIBLE);
            alertDialog = new AlertDialog.Builder(MenuActivity.this);
        }


        @Override
        protected void onProgressUpdate(String... text) {
            //finalResult.setText(text[0]);
            // Things to be done while execution of long running operation is in
            // progress. For example updating ProgessDialog
        }
    }
    public class AsyncTaskRunnerNav extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;
        AlertDialog.Builder alertDialog;

        @Override
        protected String doInBackground(String... params) {

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            if (NetworkState.isInternetAvailable_Rally()){


                try {

                    URL url = new URL("http://wsrally.cargolift.com.br:50740/api/ClassificacaoNav?rpe_codigo="+ Periodo + "&cpf="+ CPFMOT);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.connect();

                    InputStream inputStream = urlConnection.getInputStream();

                    reader = new BufferedReader(new InputStreamReader(inputStream));

                    String linha;
                    StringBuffer buffer = new StringBuffer();
                    while((linha = reader.readLine()) != null) {
                        buffer.append(linha);
                        buffer.append("\n");
                    }

                    return buffer.toString();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }

                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
                publishProgress("Sleeping..."); // Calls onProgressUpdate()
                try {
                    int time = Integer.parseInt(params[0]) * 1000;

                    Thread.sleep(time);
                    resp = "Slept for " + params[0] + " seconds";
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    resp = e.getMessage();
                } catch (Exception e) {
                    e.printStackTrace();
                    resp = e.getMessage();
                }
            }else {
                resp = "noconn";
            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            if (Objects.equals(result, "noconn")){
                new AlertDialog.Builder(MenuActivity.this)
                        .setTitle("Erro de conexão")
                        .setMessage("Houve um problema ao tentar se conectar aos nossos servidores")
                        .setPositiveButton("Tentar Novamente", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(MenuActivity.this, MenuActivity.class);
                                startActivity(intent);
                            }
                        }).setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        System.exit(0);
                    }
                }).show();
            }else {
                Gson gson = new Gson();
                Type type = new TypeToken<List<RankingNav>>() {
                }.getType();
                List<RankingNav> fromJson = gson.fromJson(result, type);

                for (RankingNav task : fromJson) {
                    if (Objects.equals(task.getCPF(), CPFMOT)) {
                        Colocacao.setText(String.valueOf(task.getColocacao()));
                        DiasTrabalhados.setText(String.valueOf(task.getTotal()));
                        Nota.setText(String.valueOf(task.getEficiencia()).substring(0, 4));
                        Nota.setGravity(Gravity.CENTER);
                        Nota2.setVisibility(View.GONE);
                        TV_PONTUALIDADE.setText("TRAJETOS");
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("Navegador", 0);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("Nome", String.valueOf(task.getNome()));
                        editor.apply();
                        name.setText(String.valueOf(task.getNome()));
                    }
                }
                // execution of result of Long time consuming operation
                progresso.setVisibility(View.GONE);
                LLperiodo.setVisibility(View.VISIBLE);
                lista.setVisibility(View.VISIBLE);
                rankNav = fromJson;
                RankNavClassAdapter adapter = new RankNavClassAdapter(rankNav, MenuActivity.this);
                lista.setAdapter(adapter);
            }
        }


        @Override
        protected void onPreExecute() {
            progresso.setVisibility(View.VISIBLE);

            alertDialog = new AlertDialog.Builder(MenuActivity.this);
        }


        @Override
        protected void onProgressUpdate(String... text) {
            //finalResult.setText(text[0]);
            // Things to be done while execution of long running operation is in
            // progress. For example updating ProgessDialog
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_ranking) {
            Intent intent = new Intent(MenuActivity.this, MenuActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_auditoria) {
            Intent intent = new Intent(MenuActivity.this, AuditoriaActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_regulamento) {
            Intent intent = new Intent(MenuActivity.this, RegulamentoActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_sair) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("Motorista", 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("Nome", null);
            editor.putString("CPF", null);
            editor.putString("DTNcmt", null);
            editor.apply();
            SharedPreferences pref2 = getApplicationContext().getSharedPreferences("Navegador", 0);
            SharedPreferences.Editor editor2 = pref2.edit();
            editor2.putString("Nome", null);
            editor2.putString("CPF", null);
            editor2.apply();
            Intent intent = new Intent(MenuActivity.this, LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

