package br.com.cargolift.rallypontualidade.View;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import br.com.cargolift.rallypontualidade.Controller.MainClassAdapter;
import br.com.cargolift.rallypontualidade.Data.ContratoDB;
import br.com.cargolift.rallypontualidade.Model.LoginModel;
import br.com.cargolift.rallypontualidade.Model.LoginNavModel;
import br.com.cargolift.rallypontualidade.Model.Motorista;
import br.com.cargolift.rallypontualidade.Model.Ranking;
import br.com.cargolift.rallypontualidade.Model.RankingNav;
import br.com.cargolift.rallypontualidade.R;
import br.com.cargolift.rallypontualidade.Sync.SyncAdapter;
import br.com.cargolift.rallypontualidade.util.Mask;
import br.com.cargolift.rallypontualidade.util.NetworkState;
import butterknife.ButterKnife;

public class LoginNavActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    EditText _emailText;
    Button _loginButton;
    SimpleDateFormat simpleDateFormat;
    TextView _signupLink;
    String email;
    ProgressBar PBlogin;
    TextInputLayout cpfinput;
    LinearLayout invisible;
    ImageView returnou;
    AsyncTaskRunner runner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginnav);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("Navegador", 0);
        String NomeNav = pref.getString("CPF", null);
        if (!Objects.equals(NomeNav, "") && NomeNav != null){
            Intent intent = new Intent(LoginNavActivity.this, MenuActivity.class);
            startActivity(intent);
        }
        PBlogin = (ProgressBar) findViewById(R.id.PBlogin);
        cpfinput = (TextInputLayout) findViewById(R.id.cpfinput);
        invisible = (LinearLayout) findViewById(R.id.invisible);


        _emailText = (EditText) findViewById(R.id.input_CPF);
        _emailText.addTextChangedListener(Mask.insert(Mask.CPF_MASK, _emailText));
        _loginButton = (Button) findViewById(R.id.btn_login);

        returnou = (ImageView) findViewById(R.id.returnou);

        Window window = this.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(LoginNavActivity.this,R.color.white));
        }

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });
        returnou.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginNavActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
        SyncAdapter.initializeSyncAdapter(this);
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        String email = _emailText.getText().toString();


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        email = _emailText.getText().toString();
        runner = new AsyncTaskRunner(email);
        String sleepTime = "30";
        runner.execute(sleepTime);

        return valid;
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;
        String cpf = "";
        String loginJSONString;


        public AsyncTaskRunner(String email) {
            cpf = email;
            cpf = cpf.replaceAll("\\.", "");
            cpf = cpf.replaceAll("-", "");


        }

        @Override
        protected String doInBackground(String... params) {
            BufferedReader reader = null;
            HttpURLConnection urlConnection = null;
            StringBuffer buffer = null;
            if (NetworkState.isInternetAvailable()){

                String[] Periodo_COLUMNS = {
                        "RPe_Codigo"
                };

                final String[] Periodo_COLUMNS2 = {
                        ContratoDB.PeriodoEntry.TABLE_NAME +"."+
                                ContratoDB.PeriodoEntry._ID,
                        ContratoDB.PeriodoEntry.RPe_Codigo,
                        ContratoDB.PeriodoEntry.RPe_Descricao,
                        ContratoDB.PeriodoEntry.RPe_Dat_Inicio,
                        ContratoDB.PeriodoEntry.RPe_Dat_Fim,
                        ContratoDB.PeriodoEntry.RPe_Data
                };

                int COL_PERIODO_ID = 0;
                int COL_PERIODO_CODIGO = 1;
                int COL_PERIODO_DESCRICAO = 2;
                int COL_PERIODO_DAT_INI = 3;
                int COL_PERIODO_DAT_FIM = 4;
                int COL_PERIODO_DAT = 5;

                Cursor cursorPeriodo;
                Uri BuscarUltimoPeriodo = ContratoDB.PeriodoEntry.buildPeriodoUri();


            try {
                cursorPeriodo = getApplicationContext().getContentResolver().query(
                        BuscarUltimoPeriodo, Periodo_COLUMNS2, null, null, "_ID ASC");

                if (cursorPeriodo != null) {
                    int contador = cursorPeriodo.getCount();


                    for (cursorPeriodo.moveToLast(); !cursorPeriodo.isBeforeFirst(); cursorPeriodo.moveToPrevious()) {
                        // This is getting the url from the string we passed in
                        String url1 = "http://wsrally.cargolift.com.br:50740/api/ClassificacaoNav?rpe_codigo="+cursorPeriodo.getString(COL_PERIODO_CODIGO)+"&cpf="+cpf ;
                        URL url = new URL(url1);

                        // Create the urlConnection
                        urlConnection = (HttpURLConnection) url.openConnection();

                        urlConnection = (HttpURLConnection) url.openConnection();
                        urlConnection.setRequestMethod("GET");
                        urlConnection.connect();

                        InputStream inputStream = urlConnection.getInputStream();

                        reader = new BufferedReader(new InputStreamReader(inputStream));

                        String linha;
                        buffer = new StringBuffer();
                        while((linha = reader.readLine()) != null) {
                            buffer.append(linha);
                            buffer.append("\n");
                        }

                        String result = buffer.toString();
                        Gson gson = new Gson();
                        Type type = new TypeToken<List<RankingNav>>() {
                        }.getType();
                        List<RankingNav> fromJson = gson.fromJson(result, type);

                        for (RankingNav task : fromJson) {
                             if (Objects.equals(task.getCPF(), cpf)) {
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("Navegador", 0);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("CPF", cpf);
                                editor.apply();
                                resp = "ok";
                                return resp;
                            }
                        }
                    }
                    resp = "NoData";
                }

            } catch (Exception e) {
                e.printStackTrace();
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }

                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }

            }
            }else {
                resp = "noconn";
            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            if (Objects.equals(result, "noconn")) {
                new AlertDialog.Builder(LoginNavActivity.this)
                        .setTitle("Erro de conexão")
                        .setMessage("Houve um problema ao tentar se conectar aos nossos servidores")
                        .setPositiveButton("Tentar Novamente", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                LoginNavActivity.AsyncTaskRunner runner = new LoginNavActivity.AsyncTaskRunner(email);
                                String sleepTime = "30";
                                runner.execute(sleepTime);
                            }
                        }).setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        System.exit(0);
                    }
                }).show();

            }
            else if (Objects.equals(result, "NoData")){
                new AlertDialog.Builder(LoginNavActivity.this)
                        .setTitle("Erro de autenticação")
                        .setMessage("Não foram encontrados dados para este CPF. Corrija os dados ou tente novamente com um novo CPF.")
                        .setPositiveButton("Tentar novamente", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                               Intent intent = new Intent(LoginNavActivity.this, LoginNavActivity.class);
                               startActivity(intent);

                                dialog.dismiss();
                            }
                        }).setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        System.exit(0);
                    }
                }).show();
            }
            else if (Objects.equals(result, "ok")){
                Intent intent = new Intent(LoginNavActivity.this, MenuActivity.class);
                startActivity(intent);


            }
        }


        @Override
        protected void onPreExecute() {
            cpfinput.setVisibility(View.GONE);
            _loginButton.setVisibility(View.GONE);
            invisible.setVisibility(View.VISIBLE);
            PBlogin.setVisibility(View.VISIBLE);

        }


        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

}
