package br.com.cargolift.rallypontualidade.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import br.com.cargolift.rallypontualidade.Data.ContratoDB.PeriodoEntry;
import br.com.cargolift.rallypontualidade.Data.ContratoDB.InconsistenciaMotEntry;
import br.com.cargolift.rallypontualidade.Data.ContratoDB.AuditoriaEntry;
import br.com.cargolift.rallypontualidade.Data.ContratoDB.ClassificacaoNavEntry;
import br.com.cargolift.rallypontualidade.Data.ContratoDB.InconsistenciaNavEntry;
import br.com.cargolift.rallypontualidade.Data.ContratoDB.ClassificacaoMotEntry;
import br.com.cargolift.rallypontualidade.Model.Inconsistencia;
import br.com.cargolift.rallypontualidade.Model.Periodo;

/**
 * Created by paulogabriel on 13/11/2017.
 */

public class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    static final String DATABASE_NAME = "rallyapp.db";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        final String SQL_CREATE_PERIODO_TABLE = "CREATE TABLE " + PeriodoEntry.TABLE_NAME + " (" +
                PeriodoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                PeriodoEntry.RPe_Codigo + " TEXT NOT NULL," +
                PeriodoEntry.RPe_Descricao + " TEXT NOT NULL, " +
                PeriodoEntry.RPe_Dat_Fim + " TEXT NOT NULL, " +
                PeriodoEntry.RPe_Data + " INTEGER NOT NULL, "+
                PeriodoEntry.RPe_Dat_Inicio + " TEXT NOT NULL " +
                " );";

        final String SQL_CREATE_AUDITORIA_TABLE = "CREATE TABLE " + AuditoriaEntry.TABLE_NAME + " (" +
                AuditoriaEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                AuditoriaEntry.Periodo + " INTEGER NOT NULL, " +
                AuditoriaEntry.Trajeto + " TEXT NOT NULL," +
                AuditoriaEntry.Placa + " TEXT NOT NULL, " +
                AuditoriaEntry.Tipo + " TEXT NOT NULL, " +
                AuditoriaEntry.Data_Prevista + " TEXT NOT NULL, "+
                AuditoriaEntry.Data_Realizada + " TEXT NOT NULL, "+
                AuditoriaEntry.Local + " TEXT NOT NULL, "+
                AuditoriaEntry.Desvio + " TEXT NOT NULL, "+
                AuditoriaEntry.Saida_Origem + " TEXT NOT NULL, "+
                AuditoriaEntry.Chegada_Destino + " TEXT NOT NULL, "+
                AuditoriaEntry.Saida_Destino + " TEXT NOT NULL, "+
                AuditoriaEntry.Velocidade + " TEXT NOT NULL, "+
                AuditoriaEntry.Velocidade_Valor + " TEXT NOT NULL, "+
                AuditoriaEntry.Picos + " TEXT NOT NULL, "+
                AuditoriaEntry.Picos_Valor + " TEXT NOT NULL, "+
                AuditoriaEntry.Trabalhados + " TEXT NOT NULL, "+
                AuditoriaEntry.Trabalhados_Valor + " TEXT NOT NULL, " +

                " FOREIGN KEY (" + AuditoriaEntry.Periodo + ") REFERENCES " +
                PeriodoEntry.TABLE_NAME + " (" + PeriodoEntry.RPe_Codigo + ") " +
                " );";

        final String SQL_CREATE_INCONSISTENCIA_MOT_TABLE = "CREATE TABLE " + InconsistenciaMotEntry.TABLE_NAME + " (" +
                InconsistenciaMotEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                InconsistenciaMotEntry.Periodo + " INTEGER NOT NULL," +
                InconsistenciaMotEntry.Placa + " TEXT NOT NULL," +
                InconsistenciaMotEntry.Frota + " TEXT NOT NULL," +
                InconsistenciaMotEntry.Previsao_Inicio + " TEXT NOT NULL," +
                InconsistenciaMotEntry.Previsao_Fim + " TEXT NOT NULL," +
                InconsistenciaMotEntry.Origem + " TEXT NOT NULL, " +
                InconsistenciaMotEntry.Destino + " TEXT NOT NULL, " +
                InconsistenciaMotEntry.Navegador + " TEXT NOT NULL, "+
                InconsistenciaMotEntry.Inconsistencia + " TEXT NOT NULL, " +

                " FOREIGN KEY (" + InconsistenciaMotEntry.Periodo + ") REFERENCES " +
                PeriodoEntry.TABLE_NAME + " (" + PeriodoEntry.RPe_Codigo + ") " +
                " );";

        final String SQL_CREATE_CLASSIFICACO_NAV_TABLE = "CREATE TABLE " + ClassificacaoNavEntry.TABLE_NAME + " (" +
                ClassificacaoNavEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                ClassificacaoNavEntry.Periodo + " INTEGER NOT NULL," +
                ClassificacaoNavEntry.Colocacao + " TEXT NOT NULL," +
                ClassificacaoNavEntry.Nome + " TEXT NOT NULL," +
                ClassificacaoNavEntry.CPF + " TEXT NOT NULL," +
                ClassificacaoNavEntry.Sem_Inconsistencia + " TEXT NOT NULL, " +
                ClassificacaoNavEntry.Com_Inconsistencia + " TEXT NOT NULL, " +
                ClassificacaoNavEntry.Total + " TEXT NOT NULL, "+
                ClassificacaoNavEntry.Eficiencia + " TEXT NOT NULL, " +


                " FOREIGN KEY (" + ClassificacaoNavEntry.Periodo + ") REFERENCES " +
                PeriodoEntry.TABLE_NAME + " (" + PeriodoEntry.RPe_Codigo + ") " +
                " );";

        final String SQL_CREATE_INCONSISTENCIA_NAV_TABLE = "CREATE TABLE " + InconsistenciaNavEntry.TABLE_NAME + " (" +
                InconsistenciaNavEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                InconsistenciaNavEntry.Periodo + " INTEGER NOT NULL," +
                InconsistenciaNavEntry.Placa + " TEXT NOT NULL," +
                InconsistenciaNavEntry.Frota + " TEXT NOT NULL," +
                InconsistenciaNavEntry.Previsao_Inicio + " TEXT NOT NULL," +
                InconsistenciaNavEntry.Previsao_Fim + " TEXT NOT NULL," +
                InconsistenciaNavEntry.Origem + " TEXT NOT NULL, " +
                InconsistenciaNavEntry.Destino + " TEXT NOT NULL, " +
                InconsistenciaNavEntry.Navegador + " INTEGER NOT NULL, "+
                InconsistenciaNavEntry.Inconsistencia + " TEXT NOT NULL, " +


                " FOREIGN KEY (" + InconsistenciaNavEntry.Periodo + ") REFERENCES " +
                PeriodoEntry.TABLE_NAME + " (" + PeriodoEntry.RPe_Codigo + ") " +
                " );";

        final String SQL_CREATE_CLASSIFICACAO_MOT_TABLE = "CREATE TABLE " + ClassificacaoMotEntry.TABLE_NAME + " (" +
                PeriodoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                ClassificacaoMotEntry.Periodo + " INTEGER NOT NULL," +
                ClassificacaoMotEntry.Colocacao + " TEXT NOT NULL," +
                ClassificacaoMotEntry.Nome + " TEXT NOT NULL," +
                ClassificacaoMotEntry.Vinculo + " TEXT NOT NULL," +
                ClassificacaoMotEntry.Trabalhados + " TEXT NOT NULL," +
                ClassificacaoMotEntry.Saida_Origem + " TEXT NOT NULL," +
                ClassificacaoMotEntry.Chegada_Destino + " TEXT NOT NULL, " +
                ClassificacaoMotEntry.Saida_Destino + " TEXT NOT NULL, " +
                ClassificacaoMotEntry.Velocidade + " INTEGER NOT NULL, "+
                ClassificacaoMotEntry.Picos + " TEXT NOT NULL, " +
                ClassificacaoMotEntry.Nota + " TEXT NOT NULL, " +


                " FOREIGN KEY (" + ClassificacaoMotEntry.Periodo + ") REFERENCES " +
                PeriodoEntry.TABLE_NAME + " (" + PeriodoEntry.RPe_Codigo + ") " +
                " );";



        sqLiteDatabase.execSQL(SQL_CREATE_PERIODO_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_AUDITORIA_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_INCONSISTENCIA_MOT_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_CLASSIFICACO_NAV_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_INCONSISTENCIA_NAV_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_CLASSIFICACAO_MOT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PeriodoEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + AuditoriaEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + InconsistenciaMotEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ClassificacaoNavEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + InconsistenciaNavEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ClassificacaoMotEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

}
