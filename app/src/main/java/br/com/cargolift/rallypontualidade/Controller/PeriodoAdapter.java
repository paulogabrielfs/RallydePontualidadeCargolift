package br.com.cargolift.rallypontualidade.Controller;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.text.ParseException;

import br.com.cargolift.rallypontualidade.View.PeriodoFragment;
import br.com.cargolift.rallypontualidade.R;
import br.com.cargolift.rallypontualidade.util.FormatarData;

/**
 * Created by paulogabriel on 13/11/2017.
 */

public class PeriodoAdapter  extends CursorAdapter {

    private static final int VIEW_TYPE_COUNT = 2;
    private static final int VIEW_TYPE_TODAY = 0;
    private static final int VIEW_TYPE_FUTURE_DAY = 1;

    // Flag to determine if we want to use a separate view for "today".
    private boolean mUseTodayLayout = true;

    /**
     * Cache of the children views for a forecast list item.
     */
    public static class ViewHolder {
        public final TextView RPe_Data_Inicio;
        public final TextView RPe_Data_fim;

        public ViewHolder(View view) {
            RPe_Data_Inicio = (TextView) view.findViewById(R.id.RPe_DataIni);
            RPe_Data_fim = (TextView) view.findViewById(R.id.RPe_DataFim);
        }
    }

    public PeriodoAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        int layoutId = R.layout.list_periodos;


        View view = LayoutInflater.from(context).inflate(layoutId, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewHolder viewHolder = (ViewHolder) view.getTag();

        String rpe_ini = null;
        try {
            rpe_ini = FormatarData.formatardata(cursor.getString(PeriodoFragment.COL_RPe_Data_Inicio));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.RPe_Data_Inicio.setText(rpe_ini);

        String rpe_fim = null;
        try {
            rpe_fim = FormatarData.formatardata(cursor.getString(PeriodoFragment.COL_RPe_Data_Fim));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.RPe_Data_fim.setText(rpe_fim);
    }

    public void setUseTodayLayout(boolean useTodayLayout) {
        mUseTodayLayout = useTodayLayout;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0 && mUseTodayLayout) ? VIEW_TYPE_TODAY : VIEW_TYPE_FUTURE_DAY;
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }
}