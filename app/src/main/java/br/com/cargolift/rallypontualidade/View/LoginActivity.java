package br.com.cargolift.rallypontualidade.View;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.text.SimpleDateFormat;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import br.com.cargolift.rallypontualidade.Controller.ViagensRealizadasAdapter;
import br.com.cargolift.rallypontualidade.Model.Auditoria;
import br.com.cargolift.rallypontualidade.Model.LoginModel;
import br.com.cargolift.rallypontualidade.Model.Motorista;
import br.com.cargolift.rallypontualidade.R;
import br.com.cargolift.rallypontualidade.Sync.SyncAdapter;
import br.com.cargolift.rallypontualidade.util.Mask;
import br.com.cargolift.rallypontualidade.util.NetworkState;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    EditText _emailText;
    TextView _passwordText;
    LinearLayout LLDt;
    Button _loginButton;
    SimpleDateFormat simpleDateFormat;
    TextView _signupLink;
    String email;
    String password;
    ProgressBar PBlogin;
    TextInputLayout cpfinput;
    LinearLayout DTNSA;
    LinearLayout invisible;
    TextView NavLogin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("Motorista", 0);
        String NomeMot = pref.getString("CPF", null);
        if (!Objects.equals(NomeMot, "") && NomeMot != null){
            Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
            startActivity(intent);
        }
        SharedPreferences pref2 = getApplicationContext().getSharedPreferences("Navegador", 0);
        String cpf = pref2.getString("CPF", null);
        if (!Objects.equals(cpf, "") && cpf != null){
            Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
            startActivity(intent);
        }
        PBlogin = (ProgressBar) findViewById(R.id.PBlogin);
        cpfinput = (TextInputLayout) findViewById(R.id.cpfinput);
        DTNSA = (LinearLayout) findViewById(R.id.DtNasc);
        invisible = (LinearLayout) findViewById(R.id.invisible);
        NavLogin = (TextView) findViewById(R.id.NavLogin);


        _emailText = (EditText) findViewById(R.id.input_CPF);
        _emailText.addTextChangedListener(Mask.insert(Mask.CPF_MASK, _emailText));
        _passwordText = (TextView) findViewById(R.id.input_password);
        LLDt = (LinearLayout) findViewById(R.id.DtNasc);
        _loginButton = (Button) findViewById(R.id.btn_login);

        Window window = this.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(LoginActivity.this,R.color.white));
        }
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        }
        LLDt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDate(1980, 0, 1, R.style.DatePickerSpinner);
            }
        });
        NavLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, LoginNavActivity.class);
                startActivity(intent);
            }
        });

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });
        SyncAdapter.initializeSyncAdapter(this);
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _loginButton.setEnabled(false);

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        email = _emailText.getText().toString();
        password = _passwordText.getText().toString();
        AsyncTaskRunner runner = new AsyncTaskRunner(email, password);
        String sleepTime = "30";
        runner.execute(sleepTime);

        return valid;
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;
        String cpf = "";
        String DTNcmt = "";
        String loginJSONString;


        public AsyncTaskRunner(String email, String password) {
            cpf = email;
            DTNcmt = password;
            cpf = cpf.replaceAll("\\.", "");
            cpf = cpf.replaceAll("-", "");

            LoginModel json = new LoginModel();
            json.setCPF(cpf);
            json.setDTNcmt(DTNcmt);

            Gson gson = new Gson();
            loginJSONString = gson.toJson(json);

        }

        @Override
        protected String doInBackground(String... params) {
            BufferedReader reader = null;
            HttpURLConnection urlConnection = null;
            StringBuffer buffer = null;
            if (NetworkState.isInternetAvailable()){
            try {
                // This is getting the url from the string we passed in
                URL url = new URL(" http://ws.cargolift.com.br:11605/rest/REST005");

                // Create the urlConnection
                urlConnection = (HttpURLConnection) url.openConnection();


                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                urlConnection.setRequestProperty("Content-Type", "application/json");

                urlConnection.setRequestMethod("POST");


                // OPTIONAL - Sets an authorization header
                urlConnection.setRequestProperty("Authorization", "Basic cGF1bG8uZ2FicmllbDpFc3RhY2FvbzE=");

                // Send the post body
                if (this.loginJSONString != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                    writer.write(loginJSONString);
                    writer.flush();
                }

                int statusCode = urlConnection.getResponseCode();
                String teste = urlConnection.getResponseMessage();

                if (statusCode ==  201) {

                    InputStream inputStream = urlConnection.getInputStream();

                    reader = new BufferedReader(new InputStreamReader(inputStream));

                    String linha;
                    buffer = new StringBuffer();
                    while((linha = reader.readLine()) != null) {
                        buffer.append(linha);
                        buffer.append("\n");
                    }

                    String result = buffer.toString();

                    Motorista motorista = new Gson().fromJson(result, Motorista.class);

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("Motorista", 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("Nome", motorista.getMotorista());
                    editor.putString("CPF", cpf);
                    editor.putString("DTNcmt", DTNcmt);
                    editor.apply();

                    Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                    startActivity(intent);
                } else {
                    resp = "noauth";
                }

            } catch (Exception e) {
                e.printStackTrace();
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }

                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }

            }
            }else {
                resp = "noconn";
            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            if (Objects.equals(result, "noconn")) {
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("Erro de conexão")
                        .setMessage("Houve um problema ao tentar se conectar aos nossos servidores")
                        .setPositiveButton("Tentar Novamente", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                AsyncTaskRunner runner = new AsyncTaskRunner(email, password);
                                String sleepTime = "30";
                                runner.execute(sleepTime);
                            }
                        }).setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        System.exit(0);
                    }
                }).show();

            }
            else if (Objects.equals(result, "noauth")){
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("Erro de autenticação")
                        .setMessage("Um ou mais parâmetros estão incorretos, verifique os dados ou solicite à Cargolift a revisão de seu cadastro.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                               Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                               startActivity(intent);

                                dialog.dismiss();
                            }
                        }).show();


            }
        }


        @Override
        protected void onPreExecute() {
            DTNSA.setVisibility(View.GONE);
            cpfinput.setVisibility(View.GONE);
            _loginButton.setVisibility(View.GONE);
            invisible.setVisibility(View.VISIBLE);
            PBlogin.setVisibility(View.VISIBLE);

        }


        @Override
        protected void onProgressUpdate(String... text) {
        }
    }
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            _passwordText.setText(simpleDateFormat.format(calendar.getTime()));
        }
    }


    @VisibleForTesting
    void showDate(int year, int monthOfYear, int dayOfMonth, int spinnerTheme) {
        new SpinnerDatePickerDialogBuilder()
                .context(LoginActivity.this)
                .callback(LoginActivity.this)
                .spinnerTheme(spinnerTheme)
                .year(year)
                .monthOfYear(monthOfYear)
                .dayOfMonth(dayOfMonth)
                .build()
                .show();
    }
}
