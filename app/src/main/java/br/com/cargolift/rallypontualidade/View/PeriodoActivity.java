package br.com.cargolift.rallypontualidade.View;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import br.com.cargolift.rallypontualidade.Model.Periodo;
import br.com.cargolift.rallypontualidade.R;
import br.com.cargolift.rallypontualidade.Sync.SyncAdapter;

public class PeriodoActivity extends AppCompatActivity implements PeriodoFragment.Callback {

    private final String LOG_TAG = PeriodoActivity.class.getSimpleName();
    private static final String DETAILFRAGMENT_TAG = "DFTAG";

    private boolean mTwoPane;
    private String mLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_periodo);

        PeriodoFragment periodoFragment =  ((PeriodoFragment)getSupportFragmentManager()
                .findFragmentById(R.id.fragment_periodo));

        SyncAdapter.initializeSyncAdapter(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
            PeriodoFragment ff = (PeriodoFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_periodo);
            if ( null != ff ) {
                ff.onLocationChanged();
            }
    }

    @Override
    public void onItemSelected(Uri contentUri) {

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);

    }


}
