package br.com.cargolift.rallypontualidade.Controller;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Objects;

import br.com.cargolift.rallypontualidade.Model.Ranking;
import br.com.cargolift.rallypontualidade.Model.RankingNav;
import br.com.cargolift.rallypontualidade.R;


/**
 * Created by paulogabriel on 06/11/2017.
 */

public class RankNavClassAdapter extends BaseAdapter {

    private final List<RankingNav> ranking;
    private final Activity act;

    public RankNavClassAdapter(List<RankingNav> ranking, Activity act) {
        this.ranking = ranking;
        this.act = act;
    }
    @Override
    public int getCount() {
        return ranking.size();
    }

    @Override
    public Object getItem(int position) {
        return ranking.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.list_ranking_nav_personalizado, parent, false);

        RankingNav rank = ranking.get(position);

        TextView tv_colocacao = (TextView)
                view.findViewById(R.id.tv_colocacao);
        ImageView iv_colocacao_um = (ImageView)
                view.findViewById(R.id.iv_colocacao_um);
        ImageView iv_colocacao_dois = (ImageView)
                view.findViewById(R.id.iv_colocacao_dois);
        TextView tv_nome = (TextView)
                view.findViewById(R.id.tv_nome);
        TextView tv_notaum = (TextView)
                view.findViewById(R.id.tv_notaum);
        TextView tv_notadois = (TextView)
                view.findViewById(R.id.tv_notadois);

        String colocacao = String.valueOf(rank.getColocacao());

        if (Objects.equals(colocacao, "1")){

            tv_colocacao.setVisibility(View.GONE);
            iv_colocacao_dois.setImageDrawable(view.getResources().getDrawable(R.drawable.number_one));

        }
        tv_colocacao.setText(String.valueOf(rank.getColocacao()+"º"));
        tv_nome.setText(rank.getNome());

        tv_notaum.setText(String.valueOf(rank.getEficiencia()).substring(0, 4));
        tv_notaum.setGravity(Gravity.CENTER);
        tv_notadois.setVisibility(View.GONE);

        return view;
    }
}
