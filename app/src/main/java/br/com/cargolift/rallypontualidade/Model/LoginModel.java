package br.com.cargolift.rallypontualidade.Model;

import java.security.PublicKey;

/**
 * Created by paulogabriel on 20/11/2017.
 */

public class LoginModel {
    public String getCPF() {
        return cpf;
    }

    public void setCPF(String CPF) {
        this.cpf = CPF;
    }

    public String getDTNcmt() {
        return DTNcmt;
    }

    public void setDTNcmt(String DTNcmt) {
        this.DTNcmt = DTNcmt;
    }

    String cpf;
    String DTNcmt;

}
