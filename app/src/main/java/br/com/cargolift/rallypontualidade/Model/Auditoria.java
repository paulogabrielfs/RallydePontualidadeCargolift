package br.com.cargolift.rallypontualidade.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by paulogabriel on 10/11/2017.
 */

public class Auditoria implements Parcelable {

        private Double Chegada_Destino;

        private Double Saida_Destino;

        private Double Velocidade;

        private String Local;

        private Double Picos;

        private String Placa;

        private Double Trabalhados_Valor;

        private Double Desvio;

        private Double Saida_Origem;

        private Double Velocidade_Valor;

        private String Tipo;

        private Double Picos_Valor;

        private String Data_Realizada;

        private Double Trabalhados;

        private String Trajeto;

        private String Data_Prevista;

    protected Auditoria(Parcel in) {
        if (in.readByte() == 0) {
            Chegada_Destino = null;
        } else {
            Chegada_Destino = in.readDouble();
        }
        if (in.readByte() == 0) {
            Saida_Destino = null;
        } else {
            Saida_Destino = in.readDouble();
        }
        if (in.readByte() == 0) {
            Velocidade = null;
        } else {
            Velocidade = in.readDouble();
        }
        Local = in.readString();
        if (in.readByte() == 0) {
            Picos = null;
        } else {
            Picos = in.readDouble();
        }
        Placa = in.readString();
        if (in.readByte() == 0) {
            Trabalhados_Valor = null;
        } else {
            Trabalhados_Valor = in.readDouble();
        }
        if (in.readByte() == 0) {
            Desvio = null;
        } else {
            Desvio = in.readDouble();
        }
        if (in.readByte() == 0) {
            Saida_Origem = null;
        } else {
            Saida_Origem = in.readDouble();
        }
        if (in.readByte() == 0) {
            Velocidade_Valor = null;
        } else {
            Velocidade_Valor = in.readDouble();
        }
        Tipo = in.readString();
        if (in.readByte() == 0) {
            Picos_Valor = null;
        } else {
            Picos_Valor = in.readDouble();
        }
        Data_Realizada = in.readString();
        if (in.readByte() == 0) {
            Trabalhados = null;
        } else {
            Trabalhados = in.readDouble();
        }
        Trajeto = in.readString();
        Data_Prevista = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (Chegada_Destino == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(Chegada_Destino);
        }
        if (Saida_Destino == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(Saida_Destino);
        }
        if (Velocidade == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(Velocidade);
        }
        dest.writeString(Local);
        if (Picos == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(Picos);
        }
        dest.writeString(Placa);
        if (Trabalhados_Valor == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(Trabalhados_Valor);
        }
        if (Desvio == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(Desvio);
        }
        if (Saida_Origem == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(Saida_Origem);
        }
        if (Velocidade_Valor == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(Velocidade_Valor);
        }
        dest.writeString(Tipo);
        if (Picos_Valor == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(Picos_Valor);
        }
        dest.writeString(Data_Realizada);
        if (Trabalhados == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(Trabalhados);
        }
        dest.writeString(Trajeto);
        dest.writeString(Data_Prevista);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Auditoria> CREATOR = new Creator<Auditoria>() {
        @Override
        public Auditoria createFromParcel(Parcel in) {
            return new Auditoria(in);
        }

        @Override
        public Auditoria[] newArray(int size) {
            return new Auditoria[size];
        }
    };

    public Double getChegada_Destino ()
        {
            return Chegada_Destino;
        }

        public void setChegada_Destino (Double Chegada_Destino)
        {
            this.Chegada_Destino = Chegada_Destino;
        }

        public Double getSaida_Destino ()
        {
            return Saida_Destino;
        }

        public void setSaida_Destino (Double Saida_Destino)
        {
            this.Saida_Destino = Saida_Destino;
        }

        public Double getVelocidade ()
        {
            return Velocidade;
        }

        public void setVelocidade (Double Velocidade)
        {
            this.Velocidade = Velocidade;
        }

        public String getLocal ()
        {
            return Local;
        }

        public void setLocal (String Local)
        {
            this.Local = Local;
        }

        public Double getPicos ()
        {
            return Picos;
        }

        public void setPicos (Double Picos)
        {
            this.Picos = Picos;
        }

        public String getPlaca ()
        {
            return Placa;
        }

        public void setPlaca (String Placa)
        {
            this.Placa = Placa;
        }

        public Double getTrabalhados_Valor ()
        {
            return Trabalhados_Valor;
        }

        public void setTrabalhados_Valor (Double Trabalhados_Valor)
        {
            this.Trabalhados_Valor = Trabalhados_Valor;
        }

        public Double getDesvio ()
        {
            return Desvio;
        }

        public void setDesvio (Double Desvio)
        {
            this.Desvio = Desvio;
        }

        public Double getSaida_Origem ()
        {
            return Saida_Origem;
        }

        public void setSaida_Origem (Double Saida_Origem)
        {
            this.Saida_Origem = Saida_Origem;
        }

        public Double getVelocidade_Valor ()
        {
            return Velocidade_Valor;
        }

        public void setVelocidade_Valor (Double Velocidade_Valor)
        {
            this.Velocidade_Valor = Velocidade_Valor;
        }

        public String getTipo ()
        {
            return Tipo;
        }

        public void setTipo (String Tipo)
        {
            this.Tipo = Tipo;
        }

        public Double getPicos_Valor ()
        {
            return Picos_Valor;
        }

        public void setPicos_Valor (Double Picos_Valor)
        {
            this.Picos_Valor = Picos_Valor;
        }

        public String getData_Realizada ()
        {
            return Data_Realizada;
        }

        public void setData_Realizada (String Data_Realizada)
        {
            this.Data_Realizada = Data_Realizada;
        }

        public Double getTrabalhados ()
        {
            return Trabalhados;
        }

        public void setTrabalhados (Double Trabalhados)
        {
            this.Trabalhados = Trabalhados;
        }

        public String getTrajeto ()
        {
            return Trajeto;
        }

        public void setTrajeto (String Trajeto)
        {
            this.Trajeto = Trajeto;
        }

        public String getData_Prevista ()
        {
            return Data_Prevista;
        }

        public void setData_Prevista (String Data_Prevista)
        {
            this.Data_Prevista = Data_Prevista;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [Chegada_Destino = "+Chegada_Destino+", Saida_Destino = "+Saida_Destino+", Velocidade = "+Velocidade+", Local = "+Local+", Picos = "+Picos+", Placa = "+Placa+", Trabalhados_Valor = "+Trabalhados_Valor+", Desvio = "+Desvio+", Saida_Origem = "+Saida_Origem+", Velocidade_Valor = "+Velocidade_Valor+", Tipo = "+Tipo+", Picos_Valor = "+Picos_Valor+", Data_Realizada = "+Data_Realizada+", Trabalhados = "+Trabalhados+", Trajeto = "+Trajeto+", Data_Prevista = "+Data_Prevista+"]";
        }
}

