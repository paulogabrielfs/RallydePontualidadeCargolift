package br.com.cargolift.rallypontualidade.Controller;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import br.com.cargolift.rallypontualidade.Model.Auditoria;
import br.com.cargolift.rallypontualidade.Model.Inconsistencia;
import br.com.cargolift.rallypontualidade.R;
import br.com.cargolift.rallypontualidade.util.FormatarData;


/**
 * Created by paulogabriel on 06/11/2017.
 */

public class ViagensDesconsideradasAdapter extends BaseAdapter {

    private final List<Inconsistencia> audit;
    private final Activity act;

    public ViagensDesconsideradasAdapter(List<Inconsistencia> audit, Activity act) {
        this.audit = audit;
        this.act = act;
    }
    @Override
    public int getCount() {
        return audit.size();
    }

    @Override
    public Object getItem(int position) {
        return audit.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.list_auditoria_viagens_desconsideradas, parent, false);

        Inconsistencia auditi = audit.get(position);

        TextView placa_frota = (TextView)
                view.findViewById(R.id.placa_frota);
        TextView data_inicio = (TextView)
                view.findViewById(R.id.data_inicio);
        TextView inconsistencia = (TextView)
                view.findViewById(R.id.inconsistencia);

        placa_frota.setText(String.valueOf(auditi.getPlaca()));

        try {
            data_inicio.setText(String.valueOf(FormatarData.formatardata(auditi.getPrevisao_Inicio())));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        inconsistencia.setText(String.valueOf(auditi.getInconsistencia()));

        return view;
    }
}
