package br.com.cargolift.rallypontualidade.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.Objects;

import br.com.cargolift.rallypontualidade.R;

public class RegulamentoActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String NomeMot;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regulamento);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        //View view = navigationView.inflateHeaderView(R.layout.nav_header_menu);
        TextView name = header.findViewById(R.id.Nome);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("Motorista", 0);
        String NomeMot2 = pref.getString("CPF", null);
        if (!Objects.equals(NomeMot2, "") && NomeMot2 != null){
            NomeMot = pref.getString("Nome", null);
            name.setText(NomeMot);
        }
        SharedPreferences pref2 = getApplicationContext().getSharedPreferences("Navegador", 0);
        String cpf = pref2.getString("CPF", null);
        if (!Objects.equals(cpf, "") && cpf != null) {
            NomeMot = pref2.getString("Nome", null);
            name.setText(NomeMot);
        }
        Menu menu = navigationView.getMenu();
        MenuItem tools1= menu.findItem(R.id.nav_ranking);
        SpannableString s1 = new SpannableString(tools1.getTitle());
        s1.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s1.length(), 0);
        tools1.setTitle(s1);
        MenuItem tools2= menu.findItem(R.id.nav_auditoria);
        SpannableString s2 = new SpannableString(tools2.getTitle());
        s2.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s2.length(), 0);
        tools2.setTitle(s2);
        MenuItem tools3= menu.findItem(R.id.nav_regulamento);
        SpannableString s3 = new SpannableString(tools3.getTitle());
        s3.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s3.length(), 0);
        tools3.setTitle(s3);
        MenuItem tools4= menu.findItem(R.id.nav_sair);
        SpannableString s4 = new SpannableString(tools4.getTitle());
        s4.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s4.length(), 0);
        tools4.setTitle(s4);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.regulamento, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_ranking) {
            Intent intent = new Intent(RegulamentoActivity.this, MenuActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_auditoria) {
            Intent intent = new Intent(RegulamentoActivity.this, AuditoriaActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_regulamento) {
            Intent intent = new Intent(RegulamentoActivity.this, RegulamentoActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_sair) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("Motorista", 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("Nome", null);
            editor.putString("CPF", null);
            editor.putString("DTNcmt", null);
            editor.apply();
            SharedPreferences pref2 = getApplicationContext().getSharedPreferences("Navegador", 0);
            SharedPreferences.Editor editor2 = pref2.edit();
            editor2.putString("Nome", null);
            editor2.putString("CPF", null);
            editor2.apply();
            Intent intent = new Intent(RegulamentoActivity.this, LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
