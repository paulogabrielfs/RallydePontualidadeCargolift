package br.com.cargolift.rallypontualidade.View;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import br.com.cargolift.rallypontualidade.Controller.MainClassAdapter;
import br.com.cargolift.rallypontualidade.Controller.ViagensDesconsideradasAdapter;
import br.com.cargolift.rallypontualidade.Controller.ViagensRealizadasAdapter;
import br.com.cargolift.rallypontualidade.Data.ContratoDB;
import br.com.cargolift.rallypontualidade.Model.Auditoria;
import br.com.cargolift.rallypontualidade.Model.Inconsistencia;
import br.com.cargolift.rallypontualidade.Model.Ranking;
import br.com.cargolift.rallypontualidade.R;
import br.com.cargolift.rallypontualidade.util.FormatarData;
import br.com.cargolift.rallypontualidade.util.NetworkState;

public class ViagensRealizadas extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private TextView finalResult;
    private Button button;
    private ListView lista;
    private List<Auditoria> audit;
    private Uri BuscarUltimoPeriodo = ContratoDB.PeriodoEntry.buildPeriodoUri();
    private Cursor cursorPeriodo;
    private Cursor cursorPeriodo2;
    private Context test;
    private String Periodo;
    private ProgressBar progresso;
    private TextView TVCArgo;
    private String CPFMOT;

    private static final String[] Periodo_COLUMNS = {
            "RPe_Codigo"
    };

    private static final String[] Periodo_COLUMNS2 = {
            ContratoDB.PeriodoEntry.TABLE_NAME +"."+
                    ContratoDB.PeriodoEntry._ID,
            ContratoDB.PeriodoEntry.RPe_Codigo,
            ContratoDB.PeriodoEntry.RPe_Descricao,
            ContratoDB.PeriodoEntry.RPe_Dat_Inicio,
            ContratoDB.PeriodoEntry.RPe_Dat_Fim,
            ContratoDB.PeriodoEntry.RPe_Data
    };

    static final int COL_PERIODO_ID = 0;
    static final int COL_PERIODO_CODIGO = 1;
    static final int COL_PERIODO_DESCRICAO = 2;
    static final int COL_PERIODO_DAT_INI = 3;
    static final int COL_PERIODO_DAT_FIM = 4;
    static final int COL_PERIODO_DAT = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viagens_realizadas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progresso = (ProgressBar) findViewById(R.id.PBprogresso);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_realizadas);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_realizadas);
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        //View view = navigationView.inflateHeaderView(R.layout.nav_header_menu);
        TextView name = header.findViewById(R.id.Nome);

        Menu menu = navigationView.getMenu();
        MenuItem tools1= menu.findItem(R.id.nav_ranking);
        SpannableString s1 = new SpannableString(tools1.getTitle());
        s1.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s1.length(), 0);
        tools1.setTitle(s1);
        MenuItem tools2= menu.findItem(R.id.nav_auditoria);
        SpannableString s2 = new SpannableString(tools2.getTitle());
        s2.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s2.length(), 0);
        tools2.setTitle(s2);
        MenuItem tools3= menu.findItem(R.id.nav_regulamento);
        SpannableString s3 = new SpannableString(tools3.getTitle());
        s3.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s3.length(), 0);
        tools3.setTitle(s3);
        MenuItem tools4= menu.findItem(R.id.nav_sair);
        SpannableString s4 = new SpannableString(tools4.getTitle());
        s4.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s4.length(), 0);
        tools4.setTitle(s4);


        SharedPreferences pref = getApplicationContext().getSharedPreferences("Motorista", 0);
        String NomeMot = pref.getString("Nome", null);
        CPFMOT= pref.getString("CPF", null);

        name.setText(NomeMot);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);

        LinearLayout LLperiodo = (LinearLayout) findViewById(R.id.LLperiodo);
        LLperiodo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ViagensRealizadas.this, PeriodoActivity.class);
                startActivityForResult(i, 1);
            }}
        );
        lista = (ListView) findViewById(R.id.ViagensRealizadas);

        SharedPreferences sp = this.getSharedPreferences("periodo", MODE_PRIVATE);

        Periodo = sp.getString("cdperiodo", "");

        if (Objects.equals(Periodo, "")){

            cursorPeriodo = getApplicationContext().getContentResolver().query(
                    BuscarUltimoPeriodo, Periodo_COLUMNS, null, null, "_ID ASC");

            int contador = cursorPeriodo.getCount();

            cursorPeriodo.moveToLast();

            Periodo = cursorPeriodo.getString(0);

        }

        String SelectItemWithCodPeriodo = ContratoDB.PeriodoEntry.RPe_Codigo + " = ?";

        String[] mSELECT_TTF_ARGUMENTS = {""};

        mSELECT_TTF_ARGUMENTS[0] = Periodo;

        cursorPeriodo2 = getApplicationContext().getContentResolver().query(
                BuscarUltimoPeriodo, Periodo_COLUMNS2, SelectItemWithCodPeriodo, mSELECT_TTF_ARGUMENTS, "_ID ASC");


        cursorPeriodo2.moveToFirst();

        try {

            String data = FormatarData.formatardata(cursorPeriodo2.getString(COL_PERIODO_DAT_INI)) + " a " + FormatarData.formatardata(cursorPeriodo2.getString(COL_PERIODO_DAT_FIM));
            TextView TV_DATA = (TextView) findViewById(R.id.TV_DATA3);

            TV_DATA.setText(data);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        ViagensRealizadas.AsyncTaskRunner runner = new AsyncTaskRunner();
        String sleepTime = "3";
        runner.execute(sleepTime);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Auditoria AuditObject = (Auditoria) lista.getItemAtPosition(position);
                TextView c = (TextView) view.findViewById(R.id.placa_frota);
                String playerChanged = c.getText().toString();

                Toast.makeText(ViagensRealizadas.this,playerChanged, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(ViagensRealizadas.this, DetalheAuditoriaActivity.class);
                intent.putExtra("tipo" ,"realizada");
                intent.putExtra("Audit" , AuditObject);
                startActivity(intent);
            }
        });


    }
    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {

            if (NetworkState.isInternetAvailable_Rally()){

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL("http://wsrally.cargolift.com.br:50740/api/Auditoria?rpe_codigo="+ Periodo +"&cpf=72118415915");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(inputStream));

                String linha;
                StringBuffer buffer = new StringBuffer();
                while((linha = reader.readLine()) != null) {
                    buffer.append(linha);
                    buffer.append("\n");
                }

                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }

                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
            publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try {
                int time = Integer.parseInt(params[0]) * 1000;

                Thread.sleep(time);
                resp = "Slept for " + params[0] + " seconds";
            } catch (InterruptedException e) {
                e.printStackTrace();
                resp = e.getMessage();
            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }
            }else {
                    resp = "noconn";
                }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            if (Objects.equals(result, "noconn")){
                new AlertDialog.Builder(ViagensRealizadas.this)
                        .setTitle("Erro de conexão")
                        .setMessage("Houve um problema ao tentar se conectar aos nossos servidores")
                        .setPositiveButton("Tentar Novamente", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent( ViagensRealizadas.this, ViagensRealizadas.class);
                                startActivity(intent);
                            }
                        }).setNegativeButton("Sair", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        System.exit(0);
                    }
                }).show();
            }else {
                Gson gson = new Gson();
                Type type = new TypeToken<List<Auditoria>>() {}.getType();
                List<Auditoria> fromJson = gson.fromJson(result, type);

                for (Auditoria task : fromJson) {
                    System.out.println(task);
                }
                progresso.setVisibility(View.GONE);
                lista.setVisibility(View.VISIBLE);
                audit = fromJson;
                ViagensRealizadasAdapter adapter = new ViagensRealizadasAdapter(audit, ViagensRealizadas.this);
                lista.setAdapter(adapter);

            }
        }


        @Override
        protected void onPreExecute() {
            progresso.setVisibility(View.VISIBLE);
            lista.setVisibility(View.INVISIBLE);
        }


        @Override
        protected void onProgressUpdate(String... text) {
            //finalResult.setText(text[0]);
            // Things to be done while execution of long running operation is in
            // progress. For example updating ProgessDialog
        }
    }
/*
    private List<Ranking> RankisTeste() {
        return new ArrayList<>(Arrays.asList(
                new Ranking(1, "Paulo Henrique Tavares da Silva", "97"),
                new Ranking(2, "Henrique Tavares da Silva", "89"),
                new Ranking(3, "Paulo Tavares da Silva", "78"),
                new Ranking(4, "Henrique da Silva", "67"),
                new Ranking(5, "Paulo da Silva", "56"),
                new Ranking(6, "Henrique Tavares", "45")
        ));
    }*/

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_realizadas);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_ranking) {
            Intent intent = new Intent(ViagensRealizadas.this, MenuActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_auditoria) {
            Intent intent = new Intent(ViagensRealizadas.this, AuditoriaActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_regulamento) {
            Intent intent = new Intent(ViagensRealizadas.this, RegulamentoActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_sair) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("Motorista", 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("Nome", null);
            editor.putString("CPF", null);
            editor.putString("DTNcmt", null);
            editor.apply();
            SharedPreferences pref2 = getApplicationContext().getSharedPreferences("Navegador", 0);
            SharedPreferences.Editor editor2 = pref2.edit();
            editor2.putString("Nome", null);
            editor2.putString("CPF", null);
            editor2.apply();
            Intent intent = new Intent(ViagensRealizadas.this, LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_realizadas);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
