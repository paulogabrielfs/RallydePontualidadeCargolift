package br.com.cargolift.rallypontualidade.Data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by paulogabriel on 13/11/2017.
 */

public class ContratoDB {

    public static final String CONTENT_AUTHORITY = "br.com.cargolift.rallypontualidade";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_PERIODO = "periodo";

    public static final String PATH_AUDITORIA = "auditoria";

    public static final String PATH_INCONSISTENCIA_MOT = "inconsistencia_mot";

    public static final String PATH_CLASSIFICACAO_NAV = "classificacao_nav";

    public static final String PATH_INCONSISTENCIA_NAV = "inconcsistencia_nav";

    public static final String PATH_CLASSIFICACAO_MOT = "classificacao_mot";

    public static final class PeriodoEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_PERIODO).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PERIODO;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PERIODO;


        public static final String RPe_Codigo = "RPe_Codigo";

        public static final String RPe_Descricao = "RPe_Descricao";

        public static final String RPe_Dat_Fim = "RPe_Dat_Fim";

        public static final String RPe_Dat_Inicio = "RPe_Dat_Inicio";

        public static final String RPe_Data = "RPe_Data";

        public static final String TABLE_NAME = "Periodo";



        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildPeriodoUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }

    public static final class AuditoriaEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_AUDITORIA).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_AUDITORIA;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_AUDITORIA;


        public static final String Periodo = "Periodo";

        public static final String Trajeto = "Trajeto";

        public static final String Placa = "Placa";

        public static final String Tipo = "Tipo";

        public static final String Data_Prevista = "Data_Prevista";

        public static final String Data_Realizada = "Data_Realizada";

        public static final String Local = "Local";

        public static final String Desvio = "Desvio";

        public static final String Saida_Origem = "Saida_Origem";

        public static final String Chegada_Destino = "Chegada_Destino";

        public static final String Saida_Destino = "Saida_Destino";

        public static final String Velocidade = "Velocidade";

        public static final String Velocidade_Valor = "Velocidade_Valor";

        public static final String Picos = "Picos";

        public static final String Picos_Valor = "Picos_Valor";

        public static final String Trabalhados = "Trabalhados";

        public static final String Trabalhados_Valor = "Trabalhados_Valor";

        public static final String TABLE_NAME = "Auditoria";



        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildPeriodoUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }

    public static final class InconsistenciaMotEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_INCONSISTENCIA_MOT).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_INCONSISTENCIA_MOT;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_INCONSISTENCIA_MOT;

        public static final String Periodo = "Periodo";

        public static final String Placa = "Placa";

        public static final String Frota = "Frota";

        public static final String Previsao_Inicio = "Previsao_Inicio";

        public static final String Previsao_Fim = "Previsao_Fim";

        public static final String Origem = "Origem";

        public static final String Destino = "Destino";

        public static final String Navegador = "Navegador";

        public static final String Inconsistencia = "Inconsistencia";

        public static final String TABLE_NAME = "Inconsistencia_mot";



        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildPeriodoUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }

    public static final class ClassificacaoNavEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CLASSIFICACAO_NAV).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CLASSIFICACAO_NAV;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CLASSIFICACAO_NAV;


        public static final String Periodo = "Periodo";

        public static final String Colocacao = "Colocacao";

        public static final String Nome = "Nome";

        public static final String CPF = "CPF";

        public static final String Sem_Inconsistencia = "Sem_Inconsistencia";

        public static final String Com_Inconsistencia = "Com_Inconsistencia";

        public static final String Total = "Total";

        public static final String Eficiencia = "Eficiencia";

        public static final String TABLE_NAME = "Classificacao_nav";



        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildPeriodoUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }

    public static final class InconsistenciaNavEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_INCONSISTENCIA_NAV).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_INCONSISTENCIA_NAV;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_INCONSISTENCIA_NAV;


        public static final String Periodo = "Periodo";

        public static final String Placa = "Placa";

        public static final String Frota = "Frota";

        public static final String Previsao_Inicio = "Previsao_Inicio";

        public static final String Previsao_Fim = "Previsao_Fim";

        public static final String Origem = "Origem";

        public static final String Destino = "Destino";

        public static final String Navegador = "Navegador";

        public static final String Inconsistencia = "Inconsistencia";

        public static final String TABLE_NAME = "Inconsistencia_nav";



        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildPeriodoUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }

    public static final class ClassificacaoMotEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CLASSIFICACAO_MOT).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CLASSIFICACAO_MOT;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CLASSIFICACAO_MOT;


        public static final String Periodo = "Periodo";

        public static final String Colocacao = "Colocacao";

        public static final String Nome = "Nome";

        public static final String Vinculo = "Vinculo";

        public static final String Trabalhados = "Trabalhados";

        public static final String Saida_Origem = "Saida_Origem";

        public static final String Chegada_Destino = "Chegada_Destino";

        public static final String Saida_Destino = "Saida_Destino";

        public static final String Velocidade = "Velocidade";

        public static final String Picos = "Picos";

        public static final String Nota = "Nota";

        public static final String TABLE_NAME = "Classificacao_mot";



        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildPeriodoUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }

}
