package br.com.cargolift.rallypontualidade.Data;

import android.annotation.TargetApi;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * Created by paulogabriel on 13/11/2017.
 */

public class ProvedorDb extends ContentProvider {

    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private DbHelper mOpenHelper;


    static final int PERIODO = 100;
    static final int AUDITORIA = 200;
    static final int INCONSISTENCIAMOT = 300;
    static final int CLASSIFICACAONAV = 400;
    static final int INCONSISTENCIANAV = 500;
    static final int CLASSIFICACAOMOT = 600;

    static UriMatcher buildUriMatcher() {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = ContratoDB.CONTENT_AUTHORITY;

        matcher.addURI(authority, ContratoDB.PATH_PERIODO, PERIODO);
        matcher.addURI(authority, ContratoDB.PATH_AUDITORIA, AUDITORIA);
        matcher.addURI(authority, ContratoDB.PATH_INCONSISTENCIA_MOT, INCONSISTENCIAMOT);
        matcher.addURI(authority, ContratoDB.PATH_INCONSISTENCIA_NAV, INCONSISTENCIANAV);
        matcher.addURI(authority, ContratoDB.PATH_CLASSIFICACAO_NAV, CLASSIFICACAONAV);
        matcher.addURI(authority, ContratoDB.PATH_CLASSIFICACAO_MOT, CLASSIFICACAOMOT);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new DbHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {

        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case PERIODO:
                return ContratoDB.PeriodoEntry.CONTENT_TYPE;
            case AUDITORIA:
                return ContratoDB.AuditoriaEntry.CONTENT_TYPE;
            case INCONSISTENCIAMOT:
                return ContratoDB.InconsistenciaMotEntry.CONTENT_TYPE;
            case INCONSISTENCIANAV:
                return ContratoDB.InconsistenciaNavEntry.CONTENT_TYPE;
            case CLASSIFICACAONAV:
                return ContratoDB.ClassificacaoNavEntry.CONTENT_TYPE;
            case CLASSIFICACAOMOT:
                return ContratoDB.ClassificacaoMotEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {
            case PERIODO:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        ContratoDB.PeriodoEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case AUDITORIA:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        ContratoDB.AuditoriaEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case INCONSISTENCIAMOT:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        ContratoDB.InconsistenciaMotEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case INCONSISTENCIANAV:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        ContratoDB.InconsistenciaNavEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case CLASSIFICACAONAV:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        ContratoDB.ClassificacaoNavEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case CLASSIFICACAOMOT:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        ContratoDB.ClassificacaoMotEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case PERIODO: {
                long _id = db.insert(ContratoDB.PeriodoEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = ContratoDB.PeriodoEntry.buildPeriodoUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case AUDITORIA: {
                long _id = db.insert(ContratoDB.AuditoriaEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = ContratoDB.AuditoriaEntry.buildPeriodoUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case INCONSISTENCIAMOT: {
                long _id = db.insert(ContratoDB.InconsistenciaMotEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = ContratoDB.InconsistenciaMotEntry.buildPeriodoUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case INCONSISTENCIANAV: {
                long _id = db.insert(ContratoDB.InconsistenciaNavEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = ContratoDB.InconsistenciaNavEntry.buildPeriodoUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case CLASSIFICACAONAV: {
                long _id = db.insert(ContratoDB.ClassificacaoNavEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = ContratoDB.ClassificacaoNavEntry.buildPeriodoUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case CLASSIFICACAOMOT: {
                long _id = db.insert(ContratoDB.ClassificacaoMotEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = ContratoDB.ClassificacaoMotEntry.buildPeriodoUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) throws UnsupportedOperationException {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        final int match = sUriMatcher.match(uri);
        int rowsDeleted;

        if (null == selection) selection = "1";

        switch (match){
            case PERIODO:
                rowsDeleted = db.delete(ContratoDB.PeriodoEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case AUDITORIA:
                rowsDeleted = db.delete(ContratoDB.AuditoriaEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case INCONSISTENCIAMOT:
                rowsDeleted = db.delete(ContratoDB.InconsistenciaMotEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case INCONSISTENCIANAV:
                rowsDeleted = db.delete(ContratoDB.InconsistenciaNavEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case CLASSIFICACAONAV:
                rowsDeleted = db.delete(ContratoDB.ClassificacaoNavEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case CLASSIFICACAOMOT:
                rowsDeleted = db.delete(ContratoDB.ClassificacaoMotEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if (rowsDeleted != 0 ){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }
    @Override
    public int update(
            Uri uri, ContentValues values, String selection, String[] selectionArgs) throws UnsupportedOperationException {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match){
            case PERIODO:
                rowsUpdated = db.update(ContratoDB.PeriodoEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case AUDITORIA:
                rowsUpdated = db.update(ContratoDB.AuditoriaEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case INCONSISTENCIAMOT:
                rowsUpdated = db.update(ContratoDB.InconsistenciaMotEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case INCONSISTENCIANAV:
                rowsUpdated = db.update(ContratoDB.InconsistenciaNavEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case CLASSIFICACAONAV:
                rowsUpdated = db.update(ContratoDB.ClassificacaoNavEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case CLASSIFICACAOMOT:
                rowsUpdated = db.update(ContratoDB.ClassificacaoMotEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int returnCount = 0;
        switch (match) {
            case PERIODO:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(ContratoDB.PeriodoEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            case AUDITORIA:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(ContratoDB.AuditoriaEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            case INCONSISTENCIAMOT:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(ContratoDB.InconsistenciaMotEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            case INCONSISTENCIANAV:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(ContratoDB.InconsistenciaNavEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            case CLASSIFICACAONAV:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(ContratoDB.ClassificacaoNavEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            case CLASSIFICACAOMOT:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(ContratoDB.ClassificacaoMotEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            default:
                return super.bulkInsert(uri, values);
        }
        return returnCount;
    }

    // You do not need to call this method. This is a method specifically to assist the testing
    // framework in running smoothly. You can read more at:
    // http://developer.android.com/reference/android/content/ContentProvider.html#shutdown()
    @Override
    @TargetApi(11)
    public void shutdown() {
        mOpenHelper.close();
        super.shutdown();
    }

}
