package br.com.cargolift.rallypontualidade.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by paulogabriel on 17/11/2017.
 */

public class FormatarDataHora {

    public static String formatardatahora(String data) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = dateFormat.parse(data);//You will get date object relative to server/client timezone wherever it is parsed
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm"); //If you need time just put specific format for time like 'HH:mm:ss'
        String dateStr = formatter.format(date);
        return dateStr;
    }

}
