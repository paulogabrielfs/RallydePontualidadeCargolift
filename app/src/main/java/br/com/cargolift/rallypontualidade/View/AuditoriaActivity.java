package br.com.cargolift.rallypontualidade.View;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import br.com.cargolift.rallypontualidade.Controller.MainClassAdapter;
import br.com.cargolift.rallypontualidade.Controller.ViagensDesconsideradasAdapter;
import br.com.cargolift.rallypontualidade.Controller.ViagensRealizadasAdapter;
import br.com.cargolift.rallypontualidade.Data.ContratoDB;
import br.com.cargolift.rallypontualidade.Model.Auditoria;
import br.com.cargolift.rallypontualidade.Model.Inconsistencia;
import br.com.cargolift.rallypontualidade.Model.Ranking;
import br.com.cargolift.rallypontualidade.R;
import br.com.cargolift.rallypontualidade.util.FormatarData;

public class AuditoriaActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private TextView finalResult;
    private Button button;
    private LinearLayout Realizadas;
    private LinearLayout Desconsideradas;
    private LinearLayout DesconsideradasNav;
    private LinearLayout LLperiodo;
    private Uri BuscarUltimoPeriodo = ContratoDB.PeriodoEntry.buildPeriodoUri();
    private Cursor cursorPeriodo;
    private Cursor cursorPeriodo2;
    private Context test;
    private String Periodo;

    private static final String[] Periodo_COLUMNS = {
            "RPe_Codigo"
    };

    private static final String[] Periodo_COLUMNS2 = {
            ContratoDB.PeriodoEntry.TABLE_NAME +"."+
                    ContratoDB.PeriodoEntry._ID,
            ContratoDB.PeriodoEntry.RPe_Codigo,
            ContratoDB.PeriodoEntry.RPe_Descricao,
            ContratoDB.PeriodoEntry.RPe_Dat_Inicio,
            ContratoDB.PeriodoEntry.RPe_Dat_Fim,
            ContratoDB.PeriodoEntry.RPe_Data
    };

    static final int COL_PERIODO_ID = 0;
    static final int COL_PERIODO_CODIGO = 1;
    static final int COL_PERIODO_DESCRICAO = 2;
    static final int COL_PERIODO_DAT_INI = 3;
    static final int COL_PERIODO_DAT_FIM = 4;
    static final int COL_PERIODO_DAT = 5;
    String NomeMot;
    String CPFMOT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auditoria);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        //View view = navigationView.inflateHeaderView(R.layout.nav_header_menu);
        TextView name = header.findViewById(R.id.Nome);

        Menu menu = navigationView.getMenu();
        MenuItem tools1= menu.findItem(R.id.nav_ranking);
        SpannableString s1 = new SpannableString(tools1.getTitle());
        s1.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s1.length(), 0);
        tools1.setTitle(s1);
        MenuItem tools2= menu.findItem(R.id.nav_auditoria);
        SpannableString s2 = new SpannableString(tools2.getTitle());
        s2.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s2.length(), 0);
        tools2.setTitle(s2);
        MenuItem tools3= menu.findItem(R.id.nav_regulamento);
        SpannableString s3 = new SpannableString(tools3.getTitle());
        s3.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s3.length(), 0);
        tools3.setTitle(s3);
        MenuItem tools4= menu.findItem(R.id.nav_sair);
        SpannableString s4 = new SpannableString(tools4.getTitle());
        s4.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s4.length(), 0);
        tools4.setTitle(s4);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);


        Realizadas = (LinearLayout) findViewById(R.id.LLViagensRealizadas);
        Desconsideradas = (LinearLayout) findViewById(R.id.LLViagensDesconsideradas);
        DesconsideradasNav = (LinearLayout) findViewById(R.id.LLViagensDesconsideradasNav);
        LLperiodo = (LinearLayout) findViewById(R.id.LLperiodo);


        SharedPreferences pref = getApplicationContext().getSharedPreferences("Motorista", 0);
        String NomeMot2 = pref.getString("CPF", null);
        if (!Objects.equals(NomeMot2, "") && NomeMot2 != null){
            NomeMot = pref.getString("Nome", null);
            CPFMOT = pref.getString("CPF",null);
            name.setText(NomeMot);
            DesconsideradasNav.setVisibility(View.GONE);
        }
        SharedPreferences pref2 = getApplicationContext().getSharedPreferences("Navegador", 0);
        String cpf = pref2.getString("CPF", null);
        if (!Objects.equals(cpf, "") && cpf != null) {
            NomeMot = pref.getString("Nome", null);
            CPFMOT = pref.getString("CPF", null);
            name.setText(NomeMot);
            Realizadas.setVisibility(View.GONE);
            Desconsideradas.setVisibility(View.GONE);

        }

        Realizadas.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AuditoriaActivity.this, ViagensRealizadas.class);
                startActivity(i);
            }}
        );
        Desconsideradas.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AuditoriaActivity.this, ViagensDesconsideradas.class);
                startActivity(i);
            }}
        );
        DesconsideradasNav.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AuditoriaActivity.this, ViagensDesconsideradasNav.class);
                startActivity(i);
            }}
        );
        LLperiodo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AuditoriaActivity.this, PeriodoActivity.class);
                startActivityForResult(i, 1);
            }}
        );
        SharedPreferences sp = this.getSharedPreferences("periodo", MODE_PRIVATE);

        Periodo = sp.getString("cdperiodo", "");

        if (Objects.equals(Periodo, "")){

            cursorPeriodo = getApplicationContext().getContentResolver().query(
                    BuscarUltimoPeriodo, Periodo_COLUMNS, null, null, "_ID ASC");

            int contador = cursorPeriodo.getCount();

            cursorPeriodo.moveToLast();

            Periodo = cursorPeriodo.getString(0);

        }

        String SelectItemWithCodPeriodo = ContratoDB.PeriodoEntry.RPe_Codigo + " = ?";

        String[] mSELECT_TTF_ARGUMENTS = {""};

        mSELECT_TTF_ARGUMENTS[0] = Periodo;

        cursorPeriodo2 = getApplicationContext().getContentResolver().query(
                BuscarUltimoPeriodo, Periodo_COLUMNS2, SelectItemWithCodPeriodo, mSELECT_TTF_ARGUMENTS, "_ID ASC");

        if (cursorPeriodo2 != null) {
            cursorPeriodo2.moveToFirst();
        }

        try {

            String data = FormatarData.formatardata(cursorPeriodo2.getString(COL_PERIODO_DAT_INI)) + " a " + FormatarData.formatardata(cursorPeriodo2.getString(COL_PERIODO_DAT_FIM));
            TextView TV_DATA = (TextView) findViewById(R.id.TV_DATA2);

            TV_DATA.setText(data);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_ranking) {
            Intent intent = new Intent(AuditoriaActivity.this, MenuActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_auditoria) {
            Intent intent = new Intent(AuditoriaActivity.this, AuditoriaActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_regulamento) {

        } else if (id == R.id.nav_sair) {
            Intent intent = new Intent(AuditoriaActivity.this, LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
