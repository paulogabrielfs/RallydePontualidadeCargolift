package br.com.cargolift.rallypontualidade.Model;

/**
 * Created by paulogabriel on 22/11/2017.
 */
public class InconsistenciaNav
{
    private String Frota;

    private String Previsao_Fim;

    private String Placa;

    private String Navegador;

    private String Destino;

    private String Previsao_Inicio;

    private String Inconsistencia;

    private String Origem;

    public String getFrota ()
    {
        return Frota;
    }

    public void setFrota (String Frota)
    {
        this.Frota = Frota;
    }

    public String getPrevisao_Fim ()
    {
        return Previsao_Fim;
    }

    public void setPrevisao_Fim (String Previsao_Fim)
    {
        this.Previsao_Fim = Previsao_Fim;
    }

    public String getPlaca ()
    {
        return Placa;
    }

    public void setPlaca (String Placa)
    {
        this.Placa = Placa;
    }

    public String getNavegador ()
    {
        return Navegador;
    }

    public void setNavegador (String Navegador)
    {
        this.Navegador = Navegador;
    }

    public String getDestino ()
    {
        return Destino;
    }

    public void setDestino (String Destino)
    {
        this.Destino = Destino;
    }

    public String getPrevisao_Inicio ()
    {
        return Previsao_Inicio;
    }

    public void setPrevisao_Inicio (String Previsao_Inicio)
    {
        this.Previsao_Inicio = Previsao_Inicio;
    }

    public String getInconsistencia ()
    {
        return Inconsistencia;
    }

    public void setInconsistencia (String Inconsistencia)
    {
        this.Inconsistencia = Inconsistencia;
    }

    public String getOrigem ()
    {
        return Origem;
    }

    public void setOrigem (String Origem)
    {
        this.Origem = Origem;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Frota = "+Frota+", Previsao_Fim = "+Previsao_Fim+", Placa = "+Placa+", Navegador = "+Navegador+", Destino = "+Destino+", Previsao_Inicio = "+Previsao_Inicio+", Inconsistencia = "+Inconsistencia+", Origem = "+Origem+"]";
    }
}