package br.com.cargolift.rallypontualidade.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by paulogabriel on 13/11/2017.
 */

public class Inconsistencia implements Parcelable {

    private String Frota;

    private String Previsao_Fim;

    private String Placa;

    private String Navegador;

    private String Destino;

    private String Previsao_Inicio;

    private String Inconsistencia;

    private String Origem;

    public Inconsistencia(String Placa, String Frota, String Previsao_Inicio, String Previsao_Fim, String Origem, String Destino, String Navegador, String Inconsistencia) {
        this.Placa = Placa;
        this.Frota = Frota;
        this.Previsao_Inicio = Previsao_Inicio;
        this.Previsao_Fim = Previsao_Fim;
        this.Origem = Origem;
        this.Destino = Destino;
        this.Navegador = Navegador;
        this.Inconsistencia = Inconsistencia;
    }

    protected Inconsistencia(Parcel in) {
        Frota = in.readString();
        Previsao_Fim = in.readString();
        Placa = in.readString();
        Navegador = in.readString();
        Destino = in.readString();
        Previsao_Inicio = in.readString();
        Inconsistencia = in.readString();
        Origem = in.readString();
    }

    public static final Creator<Inconsistencia> CREATOR = new Creator<Inconsistencia>() {
        @Override
        public Inconsistencia createFromParcel(Parcel in) {
            return new Inconsistencia(in);
        }

        @Override
        public Inconsistencia[] newArray(int size) {
            return new Inconsistencia[size];
        }
    };

    public String getFrota ()
    {
        return Frota;
    }

    public void setFrota (String Frota)
    {
        this.Frota = Frota;
    }

    public String getPrevisao_Fim ()
    {
        return Previsao_Fim;
    }

    public void setPrevisao_Fim (String Previsao_Fim)
    {
        this.Previsao_Fim = Previsao_Fim;
    }

    public String getPlaca ()
    {
        return Placa;
    }

    public void setPlaca (String Placa)
    {
        this.Placa = Placa;
    }

    public String getNavegador ()
    {
        return Navegador;
    }

    public void setNavegador (String Navegador)
    {
        this.Navegador = Navegador;
    }

    public String getDestino ()
    {
        return Destino;
    }

    public void setDestino (String Destino)
    {
        this.Destino = Destino;
    }

    public String getPrevisao_Inicio ()
    {
        return Previsao_Inicio;
    }

    public void setPrevisao_Inicio (String Previsao_Inicio)
    {
        this.Previsao_Inicio = Previsao_Inicio;
    }

    public String getInconsistencia ()
    {
        return Inconsistencia;
    }

    public void setInconsistencia (String Inconsistencia)
    {
        this.Inconsistencia = Inconsistencia;
    }

    public String getOrigem ()
    {
        return Origem;
    }

    public void setOrigem (String Origem)
    {
        this.Origem = Origem;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Frota = "+Frota+", Previsao_Fim = "+Previsao_Fim+", Placa = "+Placa+", Navegador = "+Navegador+", Destino = "+Destino+", Previsao_Inicio = "+Previsao_Inicio+", Inconsistencia = "+Inconsistencia+", Origem = "+Origem+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Frota);
        parcel.writeString(Previsao_Fim);
        parcel.writeString(Placa);
        parcel.writeString(Navegador);
        parcel.writeString(Destino);
        parcel.writeString(Previsao_Inicio);
        parcel.writeString(Inconsistencia);
        parcel.writeString(Origem);
    }
}
