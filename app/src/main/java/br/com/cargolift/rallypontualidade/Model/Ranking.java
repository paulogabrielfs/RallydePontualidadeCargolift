package br.com.cargolift.rallypontualidade.Model;

/**
 * Created by paulogabriel on 06/11/2017.
 */

public class Ranking {

    public int Colocacao;
    public String Nome;
    public String Vinculo;
    public double Trabalhados;
    public double Saida_Origem;
    public double Chegada_Destino;
    public double Saida_Destino;
    public double Velocidade;
    public double Picos;
    public double Nota;


    public Ranking(int Colocacao, String Nome, String Vinculo, double Trabalhados, double Saida_Origem, double Chegada_Destino, double Saida_Destino, double Velocidade, double Picos, double Nota) {
        this.Colocacao = Colocacao;
        this.Nome = Nome;
        this.Vinculo = Vinculo;
        this.Trabalhados = Trabalhados;
        this.Saida_Origem = Saida_Origem;
        this.Chegada_Destino = Chegada_Destino;
        this.Saida_Destino = Saida_Destino;
        this.Velocidade = Velocidade;
        this.Picos = Picos;
        this.Nota = Nota;
    }


    public int getColocacao() {
        return Colocacao;
    }

    public void setColocacao(int colocacao) {
        Colocacao = colocacao;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getVinculo() {
        return Vinculo;
    }

    public void setVinculo(String vinculo) {
        Vinculo = vinculo;
    }

    public double getTrabalhados() {
        return Trabalhados;
    }

    public void setTrabalhados(double trabalhados) {
        Trabalhados = trabalhados;
    }

    public double getSaida_Origem() {
        return Saida_Origem;
    }

    public void setSaida_Origem(double saida_Origem) {
        Saida_Origem = saida_Origem;
    }

    public double getChegada_Destino() {
        return Chegada_Destino;
    }

    public void setChegada_Destino(double chegada_Destino) {
        Chegada_Destino = chegada_Destino;
    }

    public double getSaida_Destino() {
        return Saida_Destino;
    }

    public void setSaida_Destino(double saida_Destino) {
        Saida_Destino = saida_Destino;
    }

    public double getVelocidade() {
        return Velocidade;
    }

    public void setVelocidade(double velocidade) {
        Velocidade = velocidade;
    }

    public double getPicos() {
        return Picos;
    }

    public void setPicos(double picos) {
        Picos = picos;
    }

    public double getNota() {
        return Nota;
    }

    public void setNota(double nota) {
        Nota = nota;
    }
}
