package br.com.cargolift.rallypontualidade.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.Objects;

import br.com.cargolift.rallypontualidade.Model.Auditoria;
import br.com.cargolift.rallypontualidade.Model.Inconsistencia;
import br.com.cargolift.rallypontualidade.R;
import br.com.cargolift.rallypontualidade.util.FormatarData;
import br.com.cargolift.rallypontualidade.util.FormatarDataHora;

public class DetalheAuditoriaNav extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String tipo;

    Auditoria audit;
    Inconsistencia incons;
    TextView frota;
    TextView placa;
    TextView origem;
    TextView destino;
    TextView prev_inicio;
    TextView prev_fim;
    TextView inconsistencia;
    TextView navegador;
    TextView saidaorigem;
    TextView chegadadestino;
    TextView saidadestino;
    TextView desvio;
    TextView velocidade;
    TextView velocidadevalor;
    TextView picos;
    TextView picosvalor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_auditoria_nav);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        //View view = navigationView.inflateHeaderView(R.layout.nav_header_menu);
        TextView name = header.findViewById(R.id.Nome);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("Navegador", 0);
        String NomeMot = pref.getString("Nome", null);

        name.setText(NomeMot);
        Menu menu = navigationView.getMenu();
        MenuItem tools1= menu.findItem(R.id.nav_ranking);
        SpannableString s1 = new SpannableString(tools1.getTitle());
        s1.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s1.length(), 0);
        tools1.setTitle(s1);
        MenuItem tools2= menu.findItem(R.id.nav_auditoria);
        SpannableString s2 = new SpannableString(tools2.getTitle());
        s2.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s2.length(), 0);
        tools2.setTitle(s2);
        MenuItem tools3= menu.findItem(R.id.nav_regulamento);
        SpannableString s3 = new SpannableString(tools3.getTitle());
        s3.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s3.length(), 0);
        tools3.setTitle(s3);
        MenuItem tools4= menu.findItem(R.id.nav_sair);
        SpannableString s4 = new SpannableString(tools4.getTitle());
        s4.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s4.length(), 0);
        tools4.setTitle(s4);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);

        frota = (TextView) findViewById(R.id.frota);
        placa = (TextView) findViewById(R.id.placa);
        origem = (TextView) findViewById(R.id.origem);
        destino = (TextView) findViewById(R.id.destino);
        prev_inicio = (TextView) findViewById(R.id.prev_inicio);
        prev_fim = (TextView) findViewById(R.id.prev_fim);
        inconsistencia = (TextView) findViewById(R.id.inconsistencia);
        navegador = (TextView) findViewById(R.id.navegador);

        ImageView IV_truck = (ImageView) findViewById(R.id.IV_truck);

        ScrollView v_desconsideradas =(ScrollView) findViewById(R.id.tp_inconsistencia);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

            v_desconsideradas.setVisibility(View.VISIBLE);
            incons = intent.getParcelableExtra("incons");

            frota.setText(incons.getFrota());
            placa.setText(incons.getPlaca());
            origem.setText(incons.getOrigem());
            destino.setText(incons.getDestino());
            try {
                prev_inicio.setText(FormatarDataHora.formatardatahora(incons.getPrevisao_Inicio()));
                prev_fim.setText(FormatarDataHora.formatardatahora(incons.getPrevisao_Fim()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            inconsistencia.setText(incons.getInconsistencia());
            navegador.setText(incons.getNavegador()
            );
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_ranking) {
            Intent intent = new Intent(DetalheAuditoriaNav.this, MenuActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_auditoria) {
            Intent intent = new Intent(DetalheAuditoriaNav.this, AuditoriaActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_regulamento) {
            Intent intent = new Intent(DetalheAuditoriaNav.this, RegulamentoActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_sair) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("Motorista", 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("Nome", null);
            editor.putString("CPF", null);
            editor.putString("DTNcmt", null);
            editor.apply();
            SharedPreferences pref2 = getApplicationContext().getSharedPreferences("Navegador", 0);
            SharedPreferences.Editor editor2 = pref2.edit();
            editor2.putString("Nome", null);
            editor2.putString("CPF", null);
            editor2.apply();
            Intent intent = new Intent(DetalheAuditoriaNav.this, LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
