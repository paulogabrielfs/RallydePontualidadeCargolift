package br.com.cargolift.rallypontualidade.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

/**
 * Created by paulogabriel on 21/11/2017.
 */

public class NetworkState {


    public static boolean isInternetAvailable() {
    try {
        URL myUrl = new URL("http://ws.cargolift.com.br:11605");
        URLConnection connection = myUrl.openConnection();
        connection.setConnectTimeout(30000);
        connection.connect();
        return true;
    } catch (UnknownHostException e) {
        return false;
        // Log error
    } catch (IOException e) {

        e.printStackTrace();
        return false;
    }
}
    public static boolean isInternetAvailable_Rally() {
        try {
            URL myUrl = new URL("http://wsrally.cargolift.com.br:50740");
            URLConnection connection = myUrl.openConnection();
            connection.connect();
            return true;
        } catch (UnknownHostException e) {
            return false;
            // Log error
        } catch (IOException e) {

            e.printStackTrace();
            return false;
        }
    }
}
