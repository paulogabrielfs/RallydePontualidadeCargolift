package br.com.cargolift.rallypontualidade.Model;

/**
 * Created by paulogabriel on 13/11/2017.
 */

public class Periodo {

    private String RPe_Codigo;

    private String RPe_Descricao;

    private String RPe_Dat_Fim;

    private String RPe_Dat_Inicio;

    public String getRPe_Codigo ()
    {
        return RPe_Codigo;
    }

    public void setRPe_Codigo (String RPe_Codigo)
    {
        this.RPe_Codigo = RPe_Codigo;
    }

    public String getRPe_Descricao ()
    {
        return RPe_Descricao;
    }

    public void setRPe_Descricao (String RPe_Descricao)
    {
        this.RPe_Descricao = RPe_Descricao;
    }

    public String getRPe_Dat_Fim ()
    {
        return RPe_Dat_Fim;
    }

    public void setRPe_Dat_Fim (String RPe_Dat_Fim)
    {
        this.RPe_Dat_Fim = RPe_Dat_Fim;
    }

    public String getRPe_Dat_Inicio ()
    {
        return RPe_Dat_Inicio;
    }

    public void setRPe_Dat_Inicio (String RPe_Dat_Inicio)
    {
        this.RPe_Dat_Inicio = RPe_Dat_Inicio;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [RPe_Codigo = "+RPe_Codigo+", RPe_Descricao = "+RPe_Descricao+", RPe_Dat_Fim = "+RPe_Dat_Fim+", RPe_Dat_Inicio = "+RPe_Dat_Inicio+"]";
    }
}
